<?php
require_once __DIR__ . '/vendor/autoload.php';

use Dotenv\Dotenv;

if (class_exists('Dotenv\Dotenv') && file_exists(base_path().'/.env')) {
    if (method_exists('Dotenv\Dotenv', 'createUnsafeImmutable')) {
        Dotenv::createUnsafeImmutable(base_path())->load();
    } else {
        Dotenv::createMutable(base_path())->load();
    }
}

return
[
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_environment' => 'development',
        'production' => [
            'adapter' => env('DB_CONNECTION','mysql'),
            'host' => env('DB_HOST','localhost'),
            'name' => env('DB_DATABASE','db_name'),
            'user' => env('DB_USERNAME','root'),
            'pass' => env('DB_PASSWORD',''),
            'port' => env('DB_PORT','3306'),
            'charset' => 'utf8',
        ],
        'development' => [
            'adapter' => env('DB_CONNECTION','mysql'),
            'host' => env('DB_HOST','localhost'),
            'name' => env('DB_DATABASE','db_name'),
            'user' => env('DB_USERNAME','root'),
            'pass' => env('DB_PASSWORD',''),
            'port' => env('DB_PORT','3306'),
            'charset' => 'utf8',
        ],
        'testing' => [
            'adapter' => env('DB_CONNECTION','mysql'),
            'host' => env('DB_HOST','localhost'),
            'name' => env('DB_DATABASE','db_name'),
            'user' => env('DB_USERNAME','root'),
            'pass' => env('DB_PASSWORD',''),
            'port' => env('DB_PORT','3306'),
            'charset' => 'utf8',
        ]
    ],
    'version_order' => 'creation'
];
