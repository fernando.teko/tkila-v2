<?php
namespace support;

use Firebase\JWT\JWT;

class AuthJWT{
    private static $secret_key = 'TKilaAPI_V2';
    private static $encrypt = ['HS256'];
    private static $aud = null;
    private static $expires = (60 * 60 * 24 * 7); //1 día

    public static function signIn($data) {
        $time = time();

        $token = array(
            'exp' => $time + self::$expires,
            // 'aud' => self::Aud(),
            'data' => $data
        );

        return JWT::encode($token, self::$secret_key);
    }

    public static function Check($token) {
        if(empty($token)) {
            throw new \Exception("Invalid token supplied.");
        }

        return $decode = JWT::decode(
            $token,
            self::$secret_key,
            self::$encrypt
        );
    }

    public static function GetData($token) {
        return JWT::decode(
            $token,
            self::$secret_key,
            self::$encrypt
        )->data;
    }

    private static function Aud() {
        $aud = '';

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $aud = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $aud = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $aud = $_SERVER['REMOTE_ADDR'];
        }

        $aud .= @$_SERVER['HTTP_USER_AGENT'];
        $aud .= gethostname();

        return sha1($aud);
    }
}