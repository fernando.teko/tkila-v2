<?php
namespace support;

use Rakit\Validation\Validator;

class RequestValidation extends Validator{

    /**
     * Constructor
     *
     * @param array $messages
     * @return void
     */
    public function __construct(
        array $messages = [
            'required' => 'El campo :attribute es requerido',
            'required_if' => 'El campo :attribute es requerido',
            'required_unless' => 'El campo :attribute es requerido',
            'required_with' => 'El campo :attribute es requerido',
            'required_without' => 'El campo :attribute es requerido',
            'required_with_all' => 'El campo :attribute es requerido',
            'required_without_all' => 'El campo :attribute es requerido',
            'email' => 'El campo :attribute no es un email valido',
            'alpha' => 'El campo :attribute solo permite caracteres alfabéticos',
            'numeric' => 'El campo :attribute debe ser numérico',
            'alpha_num' => 'El campo :attribute solo permite caracteres alfanuméricos',
            'alpha_dash' => 'El campo :attribute solo permite a-z, 0-9, _ y -',
            'alpha_spaces' => 'El campo :attribute solo permite caracteres alfabéticos y espacios',
            'in' => 'El campo :attribute solo permite :allowed_values',
            'not_in' => 'El campo :attribute no permite :disallowed_values',
            'min' => 'El valor mínimo del campo :attribute es :min',
            'max' => 'El valor máximo del campo :attribute es :max',
            'between' => 'El campo :attribute debe estar entre :min y :max',
            'url' => 'El campo :attribute no es una url válida',
            'integer' => 'El campo :attribute debe ser de tipo entero',
            'boolean' => 'El campo :attribute debe ser de tipo boolean',
            'ip' => 'El campo :attribute no es una IP válida',
            'ipv4' => 'El campo :attribute no es una IPv4 válida',
            'ipv6' => 'El campo :attribute no es una IPv6 válida',
            'extension' => 'El campo :attribute debe ser :allowed_extensions',
            'array' => 'El campo :attribute debe ser un arreglo',
            'same' => 'El campo :attribute debe ser el mismo que :field',
            'regex' => 'El campo :attribute tiene un formato no válido',
            'date' => 'El campo :attribute no es una fecha válida',
            'accepted' => 'El campo :attribute debe ser aceptado',
            'present' => 'El campo :attribute debe estar presente',
            'different' => 'El campo :attribute debe ser diferente que :field',
            'uploaded_file' => 'El campo :attribute no se puede subir',
            'mimes' => 'El campo :attribute debe ser :allowed_types',
            'callback' => 'El campo :attribute no es válido',
            'before' => 'El campo :attribute debe ser una fecha antes de :time',
            'after' => 'El campo :attribute debe ser una fecha despues de :time',
            'lowercase' => 'El campo :attribute deben ser minúsculas',
            'uppercase' => 'El campo :attribute deben ser mayúsculas',
            'digits' => 'El campo :attribute debe ser númerico y tener exactamente :length digitos',
            'digits_between' => 'El campo :attribute debe tener entre :min y :max digitos',
            'defaults' => 'El campo :attribute por defecto es :default',
            'default' => 'El campo :attribute por defecto es :default'
        ]
    )
    {
        $this->messages = $messages;
        $this->registerBaseValidators();
    }
}