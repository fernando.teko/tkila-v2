<?php
/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace support;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    //protected $table = 'table_name';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['fields, ...'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    //protected $guarded = [];

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    //protected $primaryKey = 'model_id';

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    //public $incrementing = false;

    /**
     * The data type of the auto-incrementing ID.
     *
     * @var string
     */
    //protected $keyType = 'string';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    //public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    //protected $dateFormat = 'U';

    // const CREATED_AT = 'creation_date';
    // const UPDATED_AT = 'updated_date';

    /**
     * The database connection that should be used by the model.
     *
     * @var string
     */
    //protected $connection = 'sqlite';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    // protected $attributes = [
    //     'delayed' => false,
    // ];
}