<?php

namespace support;

use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Mail\SmtpMailer;

class Mailer{

    public static function send($to, $subject, $html, $attachments = []){
        $mail = new Message;
        $mail->setFrom(env('MAIL_FROM'));

        //To
        if (is_array($to)) {
            //From
            if (array_key_exists('from',$to)) {
                $to['from'] = (is_array($to['from']) ? $to['from'] : $to['from'] = [$to['from']]);
                foreach($to['from'] as $correo){
                    $mail->setFrom($correo);
                }
            }
            //To
            if(array_key_exists('to', $to)){
                $to['to'] = (is_array($to['to']) ? $to['to'] : $to['to'] = [$to['to']]);
                foreach($to['to'] as $correo){
                    $mail->addTo($correo);
                }
            } else {
                return false;
            }
            //CC
            if(array_key_exists('cc', $to)){
                $to['cc'] = (is_array($to['cc']) ? $to['cc'] : $to['cc'] = [$to['cc']]);
                foreach($to['cc'] as $correo){
                    $mail->addCc($correo);
                }
            }
            //BCC
            if(array_key_exists('bcc', $to)){
                $to['bcc'] = (is_array($to['bcc']) ? $to['bcc'] : $to['bcc'] = [$to['bcc']]);
                foreach($to['bcc'] as $correo){
                    $mail->addBcc($correo);
                }
            }
        }else {
            $to_arr = explode(',', $to);
            foreach ($to_arr as $_to){
                $mail->addTo($_to);
            }
        }
        //Subject
        $mail->setSubject($subject);
        //Message
        $mail->setHTMLBody($html);
        //Attachments
        if(count($attachments)){
            foreach($attachments as $archivo){
                $mail->addAttachment($archivo);
            }
        }
        if(env('ENVIROMENT') == 'DEV'){
            $mailer = new SmtpMailer([ 
                'host' => env('MAIL_HOST'),
                    'port' => env('MAIL_PORT'),
                    'secure' => env('MAIL_SECURITY'),
                    'username' => env('MAIL_USERNAME'),
                    'password' => env('MAIL_PASSWORD'),
            ]);
        } else {
            $mailer = new SendmailMailer();
        }
        try {
            $mailer->send($mail);
            return true;
        } catch (Exception $ex){
            return false;
        }
    }
}