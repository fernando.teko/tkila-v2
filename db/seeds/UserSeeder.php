<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $data = [
            'email' => 'admin@tekoestudio.com',
			'password' => \password_hash('password', \PASSWORD_DEFAULT),
			'username' => 'admin',
			'verified' => '1',
			'registered' => \time()
        ];
        $post = $this->table('users');
        $post->insert($data)
            ->saveData();
    }
}
