<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class UsersThrottlingTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->execute(
            "CREATE TABLE IF NOT EXISTS `users_throttling` (
                            `bucket` varchar(44) NOT NULL,
                            `tokens` float unsigned NOT NULL,
                            `replenished_at` int(10) unsigned NOT NULL,
                            `expires_at` int(10) unsigned NOT NULL,
                            PRIMARY KEY (`bucket`),
                            KEY `expires_at` (`expires_at`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;"
        );
    }
}
