<?php
declare(strict_types=1);

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

final class UsersResetsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->execute(
            "CREATE TABLE IF NOT EXISTS `users_resets` (
                            `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                            `user` int(10) unsigned NOT NULL,
                            `selector` varchar(20) NOT NULL,
                            `token` varchar(255) NOT NULL,
                            `expires` int(10) unsigned NOT NULL,
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `selector` (`selector`),
                            KEY `user_expires` (`user`,`expires`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;"
        );
    }
}
