<?php
declare(strict_types=1);

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

final class UsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->execute(
            "CREATE TABLE IF NOT EXISTS `users` (
                                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                `email` varchar(249) COLLATE utf8_general_ci NOT NULL,
                                `password` varchar(255) NOT NULL,
                                `username` varchar(100) COLLATE utf8_general_ci DEFAULT NULL,
                                `status` tinyint(2) unsigned NOT NULL DEFAULT '0',
                                `verified` tinyint(1) unsigned NOT NULL DEFAULT '0',
                                `resettable` tinyint(1) unsigned NOT NULL DEFAULT '1',
                                `roles_mask` int(10) unsigned NOT NULL DEFAULT '0',
                                `registered` int(10) unsigned NOT NULL,
                                `last_login` int(10) unsigned DEFAULT NULL,
                                `force_logout` mediumint(7) unsigned NOT NULL DEFAULT '0',
                                `role` varchar(100) DEFAULT NULL,
                                `name` varchar(255) DEFAULT NULL,
                                `lastname` varchar(255) DEFAULT NULL,
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `email` (`email`),
                                UNIQUE KEY `username` (`username`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;"
        );
    }
}
