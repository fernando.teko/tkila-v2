<?php
/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

use app\api\auth\controller\AuthController;
use Webman\Route;
use support\Request;

Route::group('/api',function () {
    Route::group('/users',function () {
        // Routes with authentication
        Route::get('',[\app\api\user\controller\UserController::class,'index'])->middleware([app\middleware\AuthCheck::class]);
        Route::post('/new',[\app\api\user\controller\UserController::class,'store'])->middleware([app\middleware\AuthCheck::class]);
        Route::get('/{id}',[\app\api\user\controller\UserController::class,'show'])->middleware([app\middleware\AuthCheck::class]);
    })->middleware([app\middleware\AuthCheck::class]);
    
    Route::group('/users',function () {
        // Routes without authentication
        Route::post('/login',[AuthController::class,'login']);
        Route::post('/validate',[AuthController::class,'validate']);
        Route::post('/request-recovery',[AuthController::class,'requestRecovery']);
    });
});

Route::fallback(function(Request $request){
    $response = $request->method() == 'OPTIONS' ? response('') : json(['error' => 404, 'message' => '404 Not Found']);
    $response->withHeaders([
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers' => 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
        'Access-Control-Allow-Credentials' => 'true'
    ]);
    return $response;
});

Route::disableDefaultRoute();





