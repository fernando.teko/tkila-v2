<?php
/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace app\middleware;

use support\AuthJWT;
use Webman\Http\Request;
use Webman\Http\Response;
use Webman\MiddlewareInterface;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\SignatureInvalidException;

/**
 * Class AuthCheck
 * @package app\middleware
 */
class AuthCheck implements MiddlewareInterface
{
    public function process(Request $request, callable $next): Response
    {
        global $jwt_token;
        $authorization = $request->header('authorization');
        if (!empty($authorization)) {
            list($token) = sscanf($authorization, 'Bearer %s');
            try {
                AuthJWT::Check($token);
                $jwt_token = $token;
            } catch(ExpiredException $ex){
                return json(['error' => true, 'message' => 'Tu sesión ha expirado, ingresa nuevamente por favor', 'reauth' => true]);
            } catch(SignatureInvalidException $ex){
                return json(['error' => true, 'message' => 'Token inválido, ingresa nuevamente por favor', 'reauth' => true]);
            } catch (\Exception $ex){
                return json(['error' => true, 'message' => $ex->getMessage(), 'reauth' => true]);
            }
        } else {
            return json(['error' => true, 'message' => 'Authorization token required', 'reauth' => true]);
        }

        /** @var Response $response */
        $response = $next($request);
        return $response;
    }
}