<?php

namespace app\command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class MakeSeederCommand extends \Symfony\Component\Console\Command\Command{
    protected static $defaultName = 'seed:create';
    protected static $defaultDescription = 'Create a new seeder file';

    /**
     * Execute the current command.
     * 
     * @return Integer
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $class = $input->getArgument('seed_name');
        
        $process = new Process(['php','vendor/bin/phinx', 'seed:create', $class]);
        $process->start();

        foreach ($process as $type => $data) {
            if ($process::OUT === $type) {
                echo $data;
            }else{
                echo $data;
            }
        }
        return self::SUCCESS;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->addArgument('seed_name',1,'Name for seeder',null);
        $this->addUsage("php tkila seed:create -e development");
    }
}