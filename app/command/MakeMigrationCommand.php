<?php

namespace app\command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class MakeMigrationCommand extends \Symfony\Component\Console\Command\Command{
    protected static $defaultName = 'migrate:create';
    protected static $defaultDescription = 'Create a new migration';

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $class = $input->getArgument('migration_name');

        $process = new Process(['php','vendor/bin/phinx', 'create', $class]);
        $process->start();

        foreach ($process as $type => $data) {
            if ($process::OUT === $type) {
                echo $data;
            }else{
                echo $data;
            }
        }
        return self::SUCCESS;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->addArgument('migration_name',1,'Name for migration',null);
        $this->addUsage("php tkila migrate:create MyNewMigration");
    }
}