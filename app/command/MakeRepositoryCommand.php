<?php

namespace app\command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Touhidurabir\StubGenerator\StubGenerator;
use Illuminate\Support\Str;

class MakeRepositoryCommand extends \Symfony\Component\Console\Command\Command{
    protected static $defaultName = 'make:repository';
    protected static $defaultDescription = 'Create a new repository';

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $class = $input->getArgument('repository_name');
        $namespace = $input->getOption('path') == NULL ? "app\\repository" : str_replace("/","\\",$input->getOption('path'));

        if ($input->getOption('path') == NULL) {
            $path = base_path()."/app/repository";
        }else{
            $path = base_path()."/".$input->getOption('path');
        }

        $repository_template = "<?php".PHP_EOL."namespace {$namespace};

use app\\repository\\BaseRepository;
use app\\cache\\BaseCache;

class {$class} extends BaseRepository{

    private function __construct(ExampleModel \$model)
    {
        parent::__construct(\$model);
    }

    public static function make(\$model)
    {
        \$model = new \$model;
        return new static(\$model);
    }
}";

        if (!file_exists($path)) {
            mkdir($path,0777,true);
        }
        $fp = fopen($path."/".$class.".php","wb");
        fwrite($fp,$repository_template);
        fclose($fp);

        $output->writeln("Repository was created successful");
        return self::SUCCESS;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setAliases(['m:r']);
        $this->addOption('path','p',4,'file path',NULL);
        $this->addArgument('repository_name',1,'Name for repository',null);
        $this->addUsage("php tkila make:repository UserRepository --path=app/api/user/repository");
    }
}