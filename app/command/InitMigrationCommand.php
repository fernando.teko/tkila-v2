<?php

namespace app\command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class MakeCacheCommand extends \Symfony\Component\Console\Command\Command{
    protected static $defaultName = 'migrate:init';
    protected static $defaultDescription = 'Initialize project for migrations';

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $process = new Process(['php','vendor/bin/phinx', 'init']);
        $process->start();

        foreach ($process as $type => $data) {
            if ($process::OUT === $type) {
                echo $data;
            }else{
                echo $data;
            }
        }
        return self::SUCCESS;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->addUsage("php tkila migrate:init");
    }
}