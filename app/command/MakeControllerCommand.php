<?php

namespace app\command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Touhidurabir\StubGenerator\StubGenerator;
use Illuminate\Support\Str;

class MakeControllerCommand extends \Symfony\Component\Console\Command\Command{
    protected static $defaultName = 'make:controller';
    protected static $defaultDescription = 'Create a new controller';

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $class = $input->getArgument('controller_name');
        $namespace = $input->getOption('path') == NULL ? "app\\controller" : str_replace("/","\\",$input->getOption('path'));

        if ($input->getOption('path') == NULL) {
            $path = base_path()."/app/controller";
        }else{
            $path = base_path()."/".$input->getOption('path');
        }

        $controller_template = "<?php".PHP_EOL."namespace {$namespace};

use support\Model;
use support\Request;

class {$class}
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request \$request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request \$request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Request \$request, \$id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request \$request, \$id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request \$request, \$id)
    {
        //
    }
}";

        if (!file_exists($path)) {
            mkdir($path,0777,true);
        }
        $fp = fopen($path."/".$class.".php","wb");
        fwrite($fp,$controller_template);
        fclose($fp);

        $output->writeln("Controller was created successful");
        return self::SUCCESS;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setAliases(['m:c']);
        $this->addOption('path','p',4,'file path',NULL);
        $this->addArgument('controller_name',1,'Name for controller',null);
        $this->addUsage("php tkila make:controller UserController --path=app/api/user/controller");
    }
}