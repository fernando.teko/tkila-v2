<?php

namespace app\command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Touhidurabir\StubGenerator\StubGenerator;
use Illuminate\Support\Str;
use Symfony\Component\Process\Process;

class MigrationBreakpointCommand extends \Symfony\Component\Console\Command\Command{
    protected static $defaultName = 'migrate:breakpoint';
    protected static $defaultDescription = 'Create breakpoint to limit rollback.';

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environment = $input->getOption('environment');

        $process_arr = array(
            'php',
            'vendor/bin/phinx',
            'breakpoint',
            '-e',
            $environment
        );
        if ($input->getOption('target') != NULL) {
            $target_arr = array(
                '-t',
                $input->getOption('target')
            );
            $process_arr = array_merge($process_arr,$target_arr);
        }

        $process = new Process($process_arr);
        $process->start();

        foreach ($process as $type => $data) {
            if ($process::OUT === $type) {
                echo $data;
            }else{
                echo $data;
            }
        }
        return self::SUCCESS;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setAliases(['migrate:breakpoint']);
        $this->addOption('environment','e',4,'Environment','development');
        $this->addOption('target','t',4,'Target',NULL);
        $this->addUsage("php tkila make:migration MyNewMigrationTable");
    }
}