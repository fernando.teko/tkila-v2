<?php

namespace app\command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Touhidurabir\StubGenerator\StubGenerator;
use Illuminate\Support\Str;

class MakeModelCommand extends \Symfony\Component\Console\Command\Command{
    protected static $defaultName = 'make:model';
    protected static $defaultDescription = 'Create a new model resource';

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $class = $input->getArgument('model_name');
        $namespace = $input->getOption('path') == NULL ? "app\\model" : str_replace("/","\\",$input->getOption('path'));

        if ($input->getOption('path') == NULL) {
            $path = base_path()."/app/model";
        }else{
            $path = base_path()."/".$input->getOption('path');
        }

        $table = $input->getOption('table') == NULL ? Str::snake($class) : $input->getOption('table');

        $model_template = "<?php".PHP_EOL."namespace {$namespace};

use support\Model;

class {$class} extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected \$table = '{$table}';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public \$timestamps = false;                            
}";

        if (!file_exists($path)) {
            mkdir($path,0777,true);
        }
        $fp = fopen($path."/".$class.".php","wb");
        fwrite($fp,$model_template);
        fclose($fp);

        $output->writeln("Model was created successful");
        return self::SUCCESS;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setAliases(['m:m']);
        $this->addOption('path','p',4,'file path',NULL);
        $this->addOption('table','t',4,'model table',NULL);
        $this->addArgument('model_name',1,'Name for model',null);
        $this->addUsage("php tkila make:model UserModel --path=app/api/user/model");
    }
}