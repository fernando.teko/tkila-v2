<?php

namespace app\command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class MigrateStatusCommand extends \Symfony\Component\Console\Command\Command{
    protected static $defaultName = 'migrate:status';
    protected static $defaultDescription = 'Show status of migrations';

    /**
     * Execute the current command.
     * 
     * @return Integer
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environment = $input->getOption('environment');

        $process = new Process(['php','vendor/bin/phinx', 'status', '-e', $environment]);
        $process->start();

        foreach ($process as $type => $data) {
            if ($process::OUT === $type) {
                echo $data;
            }else{
                echo $data;
            }
        }
        return self::SUCCESS;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->addOption('environment','e',4,'Environment','development');
        $this->addUsage("php tkila migrate:status -e development");
    }
}