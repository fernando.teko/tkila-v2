<?php

namespace app\command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Touhidurabir\StubGenerator\StubGenerator;
use Illuminate\Support\Str;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class MigrateCommand extends \Symfony\Component\Console\Command\Command{
    protected static $defaultName = 'migrate:run';
    protected static $defaultDescription = 'Migrate all tables';

    /**
     * Execute the current command.
     * 
     * @return Integer
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environment = $input->getOption('environment');
        $process = new Process(['php','vendor/bin/phinx', 'migrate', '-e', $environment]);
        $process->start();

        foreach ($process as $type => $data) {
            if ($process::OUT === $type) {
                echo $data;
            }else{
                echo $data;
            }
        }
        return self::SUCCESS;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->addOption('environment','e',4,'Environment','development');
        $this->addUsage("php tkila migrate:run -e development");
    }
}