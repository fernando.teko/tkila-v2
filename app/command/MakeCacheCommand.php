<?php

namespace app\command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Touhidurabir\StubGenerator\StubGenerator;
use Illuminate\Support\Str;

class MakeCacheCommand extends \Symfony\Component\Console\Command\Command{
    protected static $defaultName = 'make:cache';
    protected static $defaultDescription = 'Create a new cache';

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $class = $input->getArgument('cache_name');
        $namespace = $input->getOption('path') == NULL ? "app\\cache" : str_replace("/","\\",$input->getOption('path'));

        if ($input->getOption('path') == NULL) {
            $path = base_path()."/app/cache";
        }else{
            $path = base_path()."/".$input->getOption('path');
        }

        $cache_template = "<?php".PHP_EOL."namespace {$namespace};

use app\\cache\\BaseCache;

class {$class} extends BaseCache{
    public function __construct(ExampleRepository \$repository)
    {
        parent::__construct(\$repository, 'cache_key');
    }
}";

        if (!file_exists($path)) {
            mkdir($path,0777,true);
        }
        $fp = fopen($path."/".$class.".php","wb");
        fwrite($fp,$cache_template);
        fclose($fp);

        $output->writeln("Cache was created successful");
        return self::SUCCESS;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setAliases(['m:cache']);
        $this->addOption('path','p',4,'file path',NULL);
        $this->addArgument('cache_name',1,'Name for cache',null);
        $this->addUsage("php tkila make:cache UserCache --path=app/api/user/cache");
    }
}