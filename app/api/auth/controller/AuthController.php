<?php
namespace app\api\auth\controller;

use app\api\user\model\User;
use support\Request;
use support\Auth;
use support\AuthJWT;
use support\Mailer;
use support\View;
use support\RequestValidation;

class AuthController
{
    private $auth;
    public function __construct()
    {
        $this->auth = new Auth;
    }

    public function login(Request $request){
        $auth = new Auth;
        $res = $auth->tk_login($request->post('username'),$request->post('pass'),FALSE);
        if (isset($res['error'])) {
            return json(['error' => TRUE, 'message' => $res['message']]);
        }else {
            $current_user = $auth->tk_current_user();
            $data = [
                'id_user' => $current_user->id,
                'username' => $current_user->username,
                'name' => $current_user->name,
                'lastname' => $current_user->lastname,
                'role' => $current_user->role,
                'email' => $current_user->email,
                'perfil' => $current_user->perfil,
            ];
            $token = AuthJWT::SignIn($data);
            return json(['error' => FALSE, 'message' => 'Datos correctos, redireccionando a la pantalla principal...', 'token' => $token, 'user' => $data]);
        }
    }

    public function validate(Request $request){
        $token = $request->post('token');
        try {
            $user_data = AuthJWT::GetData($token);
            $current_user = User::find($user_data->id_user);
            return json(['error' => FALSE,'user' => $current_user]);
        } catch(\Firebase\JWT\ExpiredException $ex){
            return json(['error' => true, 'message' => 'Tu sesión ha expirado, ingresa nuevamente por favor', 'reauth' => true]);
        } catch(\Firebase\JWT\SignatureInvalidException $ex){
            return json(['error' => true, 'message' => 'Token inválido, ingresa nuevamente por favor', 'reauth' => true]);
        } catch (\Exception $ex){
            return json(['error' => true, 'message' => $ex->getMessage(), 'reauth' => true]);
        }
    }

    public function requestRecovery(Request $request){
        $auth = new Auth;
        $validator = new RequestValidation();
        $validation = $validator->make($request->post(), [
            'email' => 'required|email'
        ]);
        $validation->validate();
        if ($validation->fails()) {
            return json(['error' => TRUE, 'errors' => $validation->errors()->firstOfAll()]);
        }

        $email = $request->post('email');

        $response = $auth->tk_recover($email);

        if ($response['error']) {
            return json(['error' => TRUE, 'message' => $response['message'], 'res' => $response]);
        }else {
            $token = $response['token'];
            
            View::assign([
                'image_path' => $request->host().'/images/logo.png',
                'email' => $email,
                'token' => $token,
                'titulo' => 'Recuperar contraseña',
                'fecha' => date('d/m/Y h:i A'),
                'usuario' => User::where('email','=',$email)->first()->username,
                'url_activacion' => $request->host().'/recovery/'.$token,
                'nombre_proyecto' => env('APP_NAME')
            ]);
            $html = view('mail/recovery');

            $res = Mailer::send($email,'Recuperar contraseña | '.env('APP_NAME'),$html);

            return json(['error' => FALSE, 'message' => 'Se te envió un correo de recuperación, revisa tu bandeja de entrada por favor.']);
        }
    }

}