<?php
namespace app\api\user\model;

use Carbon\Carbon;
use support\Model;
use Illuminate\Database\Eloquent\Builder;

class User extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'username',
        'name',
        'lastname'
    ];

    protected $hidden = [
        'password'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function scopeEmail(Builder $query, $value)
    {
        return $query->where('email','=',$value);
    }

    public function scopeId(Builder $query, $value)
    {
        return $query->where('id','=',$value);
    }
}