<?php
namespace app\api\user\controller;

use app\api\user\model\User;
use app\repository\UserRepository;
use support\Request;
use support\Auth;
use support\RequestValidation;

class UserController
{
    protected $user_repository;
    public function __construct()
    {
        $this->user_repository = UserRepository::make(User::class);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $users = $this->user_repository->all($request = $request);
        $total = $this->user_repository->total($request);
        return json(['users' => $users, 'total' => $total]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $auth = new Auth;
        $validator = new RequestValidation();
        $validation = $validator->make($request->post(), [
            'email' => 'required|email',
            'password' => 'required',
            'username' => 'required',
            'name' => 'required',
            'lastname' => 'required',
        ]);
        $validation->validate();
        if ($validation->fails()) {
            return json($validation->errors()->firstOfAll());
        }
        $extra = [
            // Extra fields here...
            'name' => $request->post('name'),
            'lastname' => $request->post('lastname'),
        ];
        $resp = $auth->tk_register($request->post('email'),$request->post('password'),'user',$request->post('username'),true,$extra);
        if (!empty($resp['error'])) {
            return json($resp);
        }
        return json(['error' => FALSE, 'message' => 'Usuario registrado con éxito.']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, $id)
    {
        $user = User::find($id);
        return json($user);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, $id)
    {
        //
    }
}