<?php

namespace app\repository;

use app\api\user\model\User;

class UserRepository extends BaseRepository{

    private function __construct(User $user)
    {
        parent::__construct($user);
    }

    public static function make($model)
    {
        $user = new $model;
        return new static($user);
    }
}