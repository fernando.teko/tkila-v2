<?php
namespace app\cache;

use app\repository\UserRepository;

class UserCache extends BaseCache{

    public function __construct(UserRepository $repository)
    {
        parent::__construct($repository, 'users');
    }
}