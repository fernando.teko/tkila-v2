# Tkila-v2
**Instalación**

Crear proyecto
```
composer create-project tekoestudio/tkila name_for_your_new_project
```
Copiar archivo .env.example con el nombre .env
```
cp .env.example .env
```
Crear base de datos para el proyecto, y configurar variables de entorno en archivo .env para conexion con base de datos con tus credenciales
```
DB_CONNECTION=mysql

DB_HOST='localhost'

DB_PORT=3306

DB_DATABASE='db_name'

DB_USERNAME='root'

DB_PASSWORD=
```

Ejecutar comando para crear tablas en base de datos
```
php vendor/bin/phinx migrate -e development 
```
Ejecutar comando para crear un usuario
```
php vendor/bin/phinx seed:run
```
Iniciar servidor de workerman
```
php start.php start 
```
Y obtendra el siguiente resultado
```
Workerman[start.php] start in DEBUG mode
----------------------------------------- WORKERMAN -----------------------------------------
Workerman version:4.0.22          PHP version:8.0.12
------------------------------------------ WORKERS ------------------------------------------
proto   user            worker          listen                 processes    status
tcp     root            webman          http://0.0.0.0:8787    16            [OK]
tcp     root            monitor         none                   1             [OK]
---------------------------------------------------------------------------------------------
Press Ctrl+C to stop. Start success. 
```
# Tkila script

El framwork cuenta con un script que permite generar archivos para crear modelo y controladores, y con esto evitar escribir todo el archivo.
Para ejecutar este script se hace de la siguiente forma:
```
php start tkila [options] [name] [arguments]
```
**Crear modelos**<br/><br/>
Podemos ver mas información sobre estos [aquí](https://laravel.com/docs/8.x/eloquent#eloquent-model-conventions)<br/><br/>
Para generar un modelo se usa el siguiente comando:
```
php start tkila make:model User
```
Este comando genera un modelo en la carpeta app/model con el nombre User.php, pero tambien podemos enviar un tercer parametro al script donde podemos enviar una ruta diferente donde podemos guardar nuestro modelo, como se muestra a continuación: 
```
php start tkila make:model User app/api/user/model
```
**Crear controladores**<br/><br/>
Podemos ver mas información sobre estos [aquí](https://laravel.com/docs/8.x/controllers)<br/><br/>
Los controladores se generan de la misma forma pero la opción en el script de tkila cambia
```
php start tkila make:controller UserController app/api/user/model
```
